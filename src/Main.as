package
{

	import com.holloran.views.MusicPlayer;

	import flash.display.Sprite;

	[SWF( width="800", height="600", backgroundColor="0xCEECF8", frameRate="24" )]
	public class Main extends Sprite
	{


		public function Main()
		{
			super ();
			var mp:MusicPlayer = new MusicPlayer ();
			mp.x = ( stage.stageWidth / 2 ) - ( mp.width / 2 );
			mp.y = 100;
			this.addChild ( mp );

		}
	}

}
